package com.neon.diego.neontest;

import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.send.SendContract;
import com.neon.diego.neontest.send.SendContract.Interactor.TransferListener;
import com.neon.diego.neontest.send.SendPresenter;
import com.neon.diego.neontest.utils.DefaultUsers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SendPresenterTest {

    @Mock
    private SendContract.View mView;

    private SendContract.Presenter mPresenter;

    @Mock
    private SendContract.Interactor mInteractor;

    @Captor
    private ArgumentCaptor<TransferListener> mListener;

    private List<User> users;

    private User mUser;

    @Before
    public void setup() {
        mPresenter = new SendPresenter(mView, mInteractor);
        mUser = new User();
        mUser.setName("Diego Figueiredo");
        mUser.setEmail("diego@email.com");
        mUser.setPhone("11111111");
        mUser.setPhoto("34343434");
    }

    @Test
    public void testInit() {
        users = DefaultUsers.getUsers();
        when(mInteractor.getUsers()).thenReturn(users);
        mPresenter.init();
        verify(mView).startList(users);
    }

    @Test
    public void testSendMoney_Success() {
        mPresenter.sendMoney(mUser);
        verify(mInteractor).sendMoney(mListener.capture(), eq(mUser));
        verify(mView).showProgress();

        mListener.getValue().onSuccess();
        verify(mView).toastMessage(R.string.transfer_completed);
        verify(mView).dismissProgress();
    }

    @Test
    public void testSendMoney_Error() {
        mPresenter.sendMoney(mUser);
        verify(mInteractor).sendMoney(mListener.capture(), eq(mUser));
        verify(mView).showProgress();

        mListener.getValue().onError();
        verify(mView).toastMessage(R.string.transfer_error);
        verify(mView).dismissProgress();
    }

}
