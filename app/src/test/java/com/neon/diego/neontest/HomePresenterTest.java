package com.neon.diego.neontest;

import com.neon.diego.neontest.home.HomeContract;
import com.neon.diego.neontest.home.HomePresenter;
import com.neon.diego.neontest.model.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {

    @Mock
    private HomeContract.View mView;

    private HomeContract.Presenter mPresenter;

    @Mock
    private HomeContract.Interactor mInteractor;

    private User mUser;

    @Before
    public void setup() {
        mPresenter = new HomePresenter(mView, mInteractor);

        mUser = new User();
        mUser.setName("Diego Figueiredo");
        mUser.setEmail("diego@email.com");
        mUser.setPhone("11111111");
        mUser.setPhoto("34343434");
    }

    @Test
    public void testInit() {
        when(mInteractor.getUser()).thenReturn(mUser);
        mPresenter.init();
        verify(mView).setUserData(mUser.getName(), mUser.getEmail());
        verify(mView).loadImage(mUser.getPhoto());
    }

}
