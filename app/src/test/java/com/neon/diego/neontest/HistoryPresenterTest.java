package com.neon.diego.neontest;

import com.neon.diego.neontest.history.HistoryContract;
import com.neon.diego.neontest.history.HistoryContract.Interactor.TransferListener;
import com.neon.diego.neontest.history.HistoryPresenter;
import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.utils.DefaultUsers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class HistoryPresenterTest {

    @Mock
    private HistoryContract.View mView;

    private HistoryContract.Presenter mPresenter;

    @Mock
    private HistoryContract.Interactor mInteractor;

    @Captor
    private ArgumentCaptor<TransferListener> mListener;

    private List<User> mUsers;

    @Before
    public void setup() {
        mPresenter = new HistoryPresenter(mView, mInteractor);
        mUsers = DefaultUsers.getUsers();
    }

    @Test
    public void init_Success() {
        mPresenter.init();

        verify(mInteractor).getListData(mListener.capture());

        verify(mView).showProgress();

        mListener.getValue().onSuccess(mUsers);
        verify(mView).startGraph(mUsers);

        List<User> users = mInteractor.orderUsersByName(mUsers);
        verify(mView).startRecyclerView(users);
        verify(mView).dismissProgress();
    }

    @Test
    public void init_Error() {
        mPresenter.init();

        verify(mInteractor).getListData(mListener.capture());

        verify(mView).showProgress();

        mListener.getValue().onError();
        verify(mView).dismissProgress();
        verify(mView).showNothingTransfer();
    }

}
