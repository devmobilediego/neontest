package com.neon.diego.neontest.home;

import com.neon.diego.neontest.model.User;

public interface HomeContract {

    interface View {

        void loadImage(String url);

        void setUserData(String name, String email);

    }

    interface Presenter {

        void init();

        void getToken();

    }

    interface Interactor {

        User getUser();

        void getToken(User user);

    }

}
