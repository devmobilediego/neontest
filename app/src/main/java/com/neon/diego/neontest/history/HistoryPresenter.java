package com.neon.diego.neontest.history;

import com.neon.diego.neontest.model.User;

import java.util.List;

public class HistoryPresenter implements HistoryContract.Presenter,
            HistoryContract.Interactor.TransferListener {

    private HistoryContract.Interactor mInteractor;
    private HistoryContract.View mView;

    public HistoryPresenter(HistoryContract.View view,
                            HistoryContract.Interactor interactor) {
        mView = view;
        mInteractor = interactor;
    }

    @Override
    public void init() {
        mInteractor.getListData(this);
        mView.showProgress();
    }

    @Override
    public void onSuccess(List<User> users) {
        if (users.size() > 0) {
            mView.startGraph(users);

            List<User> listUsers = mInteractor.orderUsersByName(users);
            mView.startRecyclerView(listUsers);
        } else {
            mView.showNothingTransfer();
        }
        mView.dismissProgress();
    }

    @Override
    public void onError() {
        mView.dismissProgress();
        mView.showNothingTransfer();
    }

}
