package com.neon.diego.neontest.send;

import com.neon.diego.neontest.R;
import com.neon.diego.neontest.model.User;

import java.util.List;

public class SendPresenter implements SendContract.Presenter,
            SendContract.Interactor.TransferListener {

    private SendContract.View mView;
    private SendContract.Interactor mInteractor;
    private List<User> users;

    public SendPresenter(SendContract.View view, SendContract.Interactor interactor) {
        mView = view;
        mInteractor = interactor;
    }

    @Override
    public void init() {
        users = mInteractor.getUsers();
        mView.startList(users);
    }

    @Override
    public void sendMoney(User user) {
        mInteractor.sendMoney(this, user);
        mView.showProgress();
    }

    @Override
    public void onSuccess() {
        mView.toastMessage(R.string.transfer_completed);
        mView.dismissProgress();
    }

    @Override
    public void onError() {
        mView.toastMessage(R.string.transfer_error);
        mView.dismissProgress();
    }

}
