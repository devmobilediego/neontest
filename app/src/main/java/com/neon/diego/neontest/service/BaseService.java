package com.neon.diego.neontest.service;

import com.neon.diego.neontest.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseService {

    private static Retrofit getRetrofit(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static TokenService getTokenService() {
        return getRetrofit(Constants.BASE_URL).create(TokenService.class);
    }

    public static SendMoneyService sendMoneyService() {
        return getRetrofit(Constants.BASE_URL).create(SendMoneyService.class);
    }

    public static TransfersService getTransfers() {
        return getRetrofit(Constants.BASE_URL).create(TransfersService.class);
    }

}
