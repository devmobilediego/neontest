package com.neon.diego.neontest.history;

import android.content.Context;

import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.service.BaseService;
import com.neon.diego.neontest.utils.DefaultUsers;
import com.neon.diego.neontest.utils.Preferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryInteractor implements HistoryContract.Interactor {

    private Context mContext;
    private Call<List<User>> generateToken;
    private TransferListener mListener;
    private List<User> defaultUsers;

    public HistoryInteractor(Context context) {
        mContext = context;
    }

    @Override
    public void getListData(TransferListener listener) {
        mListener = listener;

        generateToken = BaseService.getTransfers().getTransfers(Preferences.getToken(mContext));
        generateToken.enqueue(getTransfers);
    }

    private Callback<List<User>> getTransfers = new Callback<List<User>>() {

        @Override
        public void onResponse(Call<List<User>> call, Response<List<User>> response) {
            if (response.isSuccessful() && response.body() != null) {
                createUserList(response.body());
            } else {
                mListener.onError();
            }
        }

        @Override
        public void onFailure(Call<List<User>> call, Throwable t) {
            mListener.onError();
        }
    };

    private void createUserList(List<User> body) {
        List<User> users = new ArrayList<>();
        defaultUsers = DefaultUsers.getUsers();

        for (User user : body) {
            if (!addToExistUserInList(users, user)) {
                User completeUser = getUserById(user.getId());
                completeUser.setHistory(user.getHistory());
                users.add(completeUser);
            }
        }
        mListener.onSuccess(users);
    }

    private boolean addToExistUserInList(List<User> users, User user) {
        for (int i = 0; i < users.size(); i++) {
            if (user.getId() == users.get(i).getId()) {
                users.get(i).setHistory(users.get(i).getHistory() + user.getHistory());
                return true;
            }
        }
        return false;
    }

    private User getUserById(int id) {
        for (int i = 0; i < defaultUsers.size(); i++) {
            if (id == defaultUsers.get(i).getId()) {
                return defaultUsers.get(i);
            }
        }
        return null;
    }

    @Override
    public List<User> orderUsersByName(List<User> users) {
        Collections.sort(users, new Comparator<User>() {
            @Override
            public int compare(User user, User t1) {
                return user.getName().compareTo(t1.getName());
            }
        });
        return users;
    }

}
