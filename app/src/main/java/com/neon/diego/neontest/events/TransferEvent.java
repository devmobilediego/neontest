package com.neon.diego.neontest.events;

import com.neon.diego.neontest.model.User;

public class TransferEvent {

    private User mUser;

    public TransferEvent(User user) {
        mUser = user;
    }

    public User getUser() {
        return mUser;
    }

}
