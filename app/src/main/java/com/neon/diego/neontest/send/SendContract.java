package com.neon.diego.neontest.send;

import com.neon.diego.neontest.model.User;

import java.util.List;

public interface SendContract {

    interface View {

        void startList(List<User> users);

        void toastMessage(int resId);

        void showProgress();

        void dismissProgress();

    }

    interface Presenter {

        void init();

        void sendMoney(User user);

    }

    interface Interactor {

        interface TransferListener {

            void onSuccess();

            void onError();

        }

        List<User> getUsers();

        void sendMoney(TransferListener listener, User user);

    }

}
