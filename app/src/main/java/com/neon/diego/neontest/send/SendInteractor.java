package com.neon.diego.neontest.send;

import android.content.Context;

import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.service.BaseService;
import com.neon.diego.neontest.utils.DefaultUsers;
import com.neon.diego.neontest.utils.Preferences;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendInteractor implements SendContract.Interactor {

    private Context mContext;

    private Call<Boolean> generateToken;
    private TransferListener mListener;

    public SendInteractor(Context context) {
        mContext = context;
    }

    @Override
    public List<User> getUsers() {
        return DefaultUsers.getUsers();
    }

    @Override
    public void sendMoney(TransferListener listener, User user) {
        mListener = listener;

        HashMap valuesRequest = new HashMap();
        valuesRequest.put("clienteId", String.valueOf(user.getId()));
        valuesRequest.put("token", Preferences.getToken(mContext));
        valuesRequest.put("valor", user.getHistory());

        generateToken = BaseService.sendMoneyService().sendMoney(valuesRequest);
        generateToken.enqueue(sendMoney);
    }

    private Callback<Boolean> sendMoney = new Callback<Boolean>() {

        @Override
        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
            if (response.body() != null) {
                boolean isSuccess = response.body();
                if (isSuccess) {
                    mListener.onSuccess();
                } else {
                    requestError();
                }
            } else {
                requestError();
            }
        }

        @Override
        public void onFailure(Call<Boolean> call, Throwable t) {
            requestError();
        }
    };

    private void requestError() {
        mListener.onError();
    }

}
