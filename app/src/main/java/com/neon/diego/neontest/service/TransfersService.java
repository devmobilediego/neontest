package com.neon.diego.neontest.service;

import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TransfersService {

    @GET(Constants.URL_TRANSFERS)
    Call<List<User>> getTransfers(@Query("token") String token);

}
