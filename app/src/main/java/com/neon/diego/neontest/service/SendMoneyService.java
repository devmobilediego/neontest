package com.neon.diego.neontest.service;

import com.neon.diego.neontest.utils.Constants;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SendMoneyService {

    @POST(Constants.URL_SEND_MONEY)
    Call<Boolean> sendMoney(@Body HashMap data);

}
