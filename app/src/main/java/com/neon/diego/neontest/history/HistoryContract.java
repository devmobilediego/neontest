package com.neon.diego.neontest.history;

import com.neon.diego.neontest.model.User;

import java.util.List;

public interface HistoryContract  {

    interface View {

        void startGraph(List<User> users);

        void startRecyclerView(List<User> users);

        void showNothingTransfer();

        void showProgress();

        void dismissProgress();

    }

    interface Presenter {

        void init();

    }

    interface Interactor {

        interface TransferListener {

            void onSuccess(List<User> users);

            void onError();

        }

        void getListData(TransferListener listener);

        List<User> orderUsersByName(List<User> users);

    }


}
