package com.neon.diego.neontest.utils;

public class Constants {

    public static final String BASE_URL = "http://processoseletivoneon.azurewebsites.net";
    public static final String URL_TOKEN = "generatetoken";
    public static final String URL_SEND_MONEY = "SendMoney";
    public static final String URL_TRANSFERS = "getTransfers";

}
