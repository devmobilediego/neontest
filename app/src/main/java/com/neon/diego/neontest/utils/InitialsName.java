package com.neon.diego.neontest.utils;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InitialsName {

    public static String getInitialsName(String name) {

        Pattern p = Pattern.compile("((^| )[A-Za-z])");
        Matcher m = p.matcher(name);
        String inititals="";
        while(m.find()){
            inititals+=m.group().trim();
        }

        return inititals.toUpperCase().substring(0, 2);
    }

}
