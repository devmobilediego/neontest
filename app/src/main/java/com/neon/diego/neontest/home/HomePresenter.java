package com.neon.diego.neontest.home;

import com.neon.diego.neontest.model.User;

public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View mView;
    private HomeContract.Interactor mInteractor;
    private User mUser;

    public HomePresenter(HomeContract.View view, HomeContract.Interactor interactor) {
        mView = view;
        mInteractor = interactor;
    }

    @Override
    public void init() {
        mUser = mInteractor.getUser();
        mView.setUserData(mUser.getName(), mUser.getEmail());
        mView.loadImage(mUser.getPhoto());
        getToken();
    }

    @Override
    public void getToken() {
        mInteractor.getToken(mUser);
    }

}
