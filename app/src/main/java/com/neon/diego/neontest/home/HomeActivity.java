package com.neon.diego.neontest.home;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.neon.diego.neontest.R;
import com.neon.diego.neontest.history.HistoryActivity;
import com.neon.diego.neontest.send.SendActivity;
import com.neon.diego.neontest.ui.widgets.CircleTransform;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements HomeContract.View {

    @BindView(R.id.profile_image)
    ImageView mProfileImage;

    @BindView(R.id.name)
    TextView mName;

    @BindView(R.id.email)
    TextView mEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        HomeInteractor interactor = new HomeInteractor(this);
        HomePresenter mPresenter = new HomePresenter(this, interactor);
        mPresenter.init();
    }

    @Override
    public void loadImage(String url) {
        Picasso.with(HomeActivity.this)
                .load(url)
                .placeholder(R.drawable.ic_person)
                .error(R.drawable.ic_person)
                .transform(new CircleTransform())
                .into(mProfileImage);
    }

    @Override
    public void setUserData(String name, String email) {
        mName.setText(name);
        mEmail.setText(email);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.send_money)
    public void sendMoney() {
        Intent intent = new Intent(this, SendActivity.class);
        startActivity(intent);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.start_history)
    public void startHistory() {
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
    }

}
