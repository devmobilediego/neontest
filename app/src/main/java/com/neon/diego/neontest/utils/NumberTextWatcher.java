package com.neon.diego.neontest.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.NumberFormat;

public class NumberTextWatcher implements TextWatcher {

    private final EditText et;
    private boolean isUpdating = false;
    private NumberFormat nf = NumberFormat.getCurrencyInstance();

    public NumberTextWatcher(EditText editText) {
        this.et = editText;
    }

    @Override
    public void afterTextChanged(Editable s) {

        if (isUpdating) {
            isUpdating = false;
            return;
        }

        isUpdating = true;

        String str = s.toString();

        boolean hasMask = ((str.indexOf("R$") > -1 || str.indexOf("$") > -1) &&
                (str.indexOf(".") > -1 || str.indexOf(",") > -1));

        if (hasMask) {
            str = str.replaceAll("[R$]", "").replaceAll("[,]", "")
                    .replaceAll("[.]", "");
        }
        try {
            str = nf.format(Double.parseDouble(str) / 100);
            et.setText(str);
            et.setSelection(et.getText().length());

        } catch (NumberFormatException e) {
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

}
