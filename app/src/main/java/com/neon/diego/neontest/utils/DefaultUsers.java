package com.neon.diego.neontest.utils;

import com.neon.diego.neontest.model.User;

import java.util.ArrayList;
import java.util.List;

public class DefaultUsers {

    public static List<User> getUsers() {

        List<User> users = new ArrayList<>();

        users.add(getUser("Anderson Santos", "(11)99999-1234", "email@email.com", "https://hmsportugal.files.wordpress.com/2011/09/dsc00560-e1322568777817.jpg", 11));
        users.add(getUser("Bianca Gente Fina", "(11)99999-4321", "email@email.com", "https://i2.wp.com/www.xapuri.info/wp-content/uploads/2017/03/tj1.png?fit=1200%2C628&ssl=1&resize=350%2C200", 22));
        users.add(getUser("Débora Pomposa", "(11)99999-4312", "email@email.com", "", 33));
        users.add(getUser("Derlene da Terra", "(11)99999-1243", "email@email.com", "https://i.em.com.br/zeXV3Ok1NNoh89ujWQ9RIQAIljk=/smart/imgsapp.em.com.br/app/noticia_127983242361/2017/08/31/896797/20170831163113840038i.jpg", 44));
        users.add(getUser("Fabiana Casca Grossa da Santa Maria de Lourdes Amém", "(11)99999-5678", "email@email.com", "", 55));
        users.add(getUser("Francisca Sabida", "(11)99999-8765", "email@email.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP0HeFRd2-fLoZGXOPlfuJ0vBt7BNLA9j2OZYG9o-A0bAPo0ie", 66));
        users.add(getUser("Gerson Mariano", "(11)99999-5687", "email@email.com", "https://pbs.twimg.com/profile_images/638413501975953408/7T7nflZL.jpg", 77));
        users.add(getUser("Goku da Silva", "(11)99999-5786", "email@email.com", "https://static.comicvine.com/uploads/original/11125/111253059/4830025-goku_ssj3___coloured_by_ezio_anime-d55csm6.jpg", 88));
        users.add(getUser("Vegeta de Freitas", "(11)99999-8576", "email@email.com", "http://sm.ign.com/ign_pt/screenshot/default/vegeta-ssgss-2_6x7c.png", 99));
        users.add(getUser("Gohan Barbosa", "(11)99999-8567", "email@email.com", "http://pm1.narvii.com/6276/8d48acd6cd35b207b3fb1de3a96df5809cb79f66_00.jpg",100));
        users.add(getUser("Monkey Luffy Nascimento", "(11)99999-9146", "email@email.com", "http://cubogeek.pt/wp-content/uploads/2016/06/cropresize-800x800-confirmada-la-muerte-del-protagonista-del-famoso-anime-one-piece.jpeg", 101));
        users.add(getUser("Roronoa Zoro Carvalho", "(11)99999-2693", "email@email.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKlsDCUsgrBlev2tubnPdnXgcTAIN2h8VZQ6l1nlH6KZlAJkwi", 102));
        users.add(getUser("Robin Lopes", "(11)99999-1792", "email@email.com", "http://pm1.narvii.com/5949/4a9b1487768b0be885142d02593ef972780431ca_hq.jpg", 103));
        users.add(getUser("Sanji Vidal", "(11)99999-1672", "email@email.com", "https://myanimelist.cdn-dena.com/s/common/uploaded_files/1476881633-b71212f7f1bbcd830e289c65e047d123.jpeg", 104));
        users.add(getUser("Nico Robin Lima", "(11)99999-8352", "email@email.com", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQSeQt5eSwIPeWbFLAtaWzWaLMdKj0Q-qz7SH8is5Skb_6DHJ42_w", 105));

        return users;
    }

    private static User getUser(String name, String phone, String email, String photo, int id) {
        User user = new User();
        user.setName(name);
        user.setPhone(phone);
        user.setEmail(email);
        user.setPhoto(photo);
        user.setId(id);
        return user;
    }

}
