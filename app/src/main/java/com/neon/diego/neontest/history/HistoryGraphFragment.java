package com.neon.diego.neontest.history;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.neon.diego.neontest.R;
import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.utils.InitialsName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryGraphFragment extends Fragment {

    public static final String ARG_USER = "user";

    private List<User> mUsers;

    @BindView(R.id.chart)
    BarChart mChart;

    public static HistoryGraphFragment newInstance(List<User> users) {
        HistoryGraphFragment fragment = new HistoryGraphFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_USER, (Serializable) users);

        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.history_graph_fragment, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUsers = (List<User>) getArguments().getSerializable(ARG_USER);
        Collections.sort(mUsers, new Comparator<User>() {
            @Override
            public int compare(User user, User t1) {
                return Double.compare(user.getHistory(), t1.getHistory());
            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        configsChart();
    }

    private void configsChart() {
        mChart.setDrawBarShadow(false);
        mChart.animateY(750);
        mChart.setMaxVisibleValueCount(12);
        mChart.setPinchZoom(false);
        mChart.setDrawGridBackground(false);
        mChart.setHighlightPerDragEnabled(false);
        mChart.setDrawValueAboveBar(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        ArrayList<BarEntry> mValues = new ArrayList<>();
        getMValues(mValues);

        ArrayList<String> xVals = new ArrayList<>();
        getXValues(xVals);

        BarDataSet dataSet = new BarDataSet(mValues, "Transferências");
        setDataSetColor(dataSet);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);

        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(10f);

        mChart.setData(data);
    }

    private void setDataSetColor(BarDataSet dataSet) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dataSet.setColor(getResources().getColor(R.color.colorPrimary, getContext().getTheme()));
        } else {
            dataSet.setColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    private void getXValues(ArrayList<String> xVals) {
        for (int i = 0; i < mUsers.size(); i++) {
            xVals.add(InitialsName.getInitialsName(mUsers.get(i).getName()));
        }
    }

    private void getMValues(ArrayList<BarEntry> mValues) {
        for (int i = 0; i < mUsers.size(); i++) {
            float value = (float) mUsers.get(i).getHistory();

            BarEntry entry = new BarEntry(value, i);
            entry.setData(mUsers.get(i));
            mValues.add(entry);
        }
    }

}
