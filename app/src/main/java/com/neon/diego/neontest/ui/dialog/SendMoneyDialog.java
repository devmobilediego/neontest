package com.neon.diego.neontest.ui.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.neon.diego.neontest.R;
import com.neon.diego.neontest.events.TransferEvent;
import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.ui.widgets.CircleTransform;
import com.neon.diego.neontest.utils.NumberTextWatcher;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class SendMoneyDialog extends DialogFragment {

    public static final String ARG_USER = "user";

    private User mUser;

    @BindView(R.id.profile_image)
    ImageView profileImage;

    @BindView(R.id.name)
    TextView mName;

    @BindView(R.id.phone)
    TextView mPhone;

    @BindView(R.id.input)
    EditText mInput;

    @BindView(R.id.send_container)
    CardView mSendContainer;

    @BindView(R.id.send_text)
    TextView mSendText;

    @BindView(R.id.progress)
    ProgressBar mProgress;

    public static SendMoneyDialog newInstance(User user) {
        SendMoneyDialog fragment = new SendMoneyDialog();

        Bundle args = new Bundle();
        args.putSerializable(ARG_USER, user);

        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.send_money_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_TITLE, 0);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Bundle args = getArguments();
        mUser = (User) args.getSerializable(ARG_USER);

        setUserData();

        mInput.addTextChangedListener(new NumberTextWatcher(mInput));
    }

    @OnClick(R.id.send_container)
    @SuppressWarnings("unused")
    public void sendMoney() {
        String value = mInput.getText().toString()
                .replace("R$", "")
                .replace(".","")
                .replace(",",".");

        mUser.setHistory(Double.parseDouble(value));
        EventBus.getDefault().post(new TransferEvent(mUser));
        closeDialog();
    }

    @OnClick(R.id.close)
    public void closeDialog() {
        getDialog().dismiss();
    }

    private void setUserData() {
        if (mUser.getPhoto() != null && !mUser.getPhoto().equals("")) {
            Picasso.with(getActivity())
                    .load(mUser.getPhoto())
                    .placeholder(R.drawable.ic_person)
                    .error(R.drawable.ic_person)
                    .transform(new CircleTransform())
                    .into(profileImage);
        } else {
            profileImage.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_person));
        }

        mName.setText(mUser.getName());
        mPhone.setText(mUser.getPhone());
    }

}
