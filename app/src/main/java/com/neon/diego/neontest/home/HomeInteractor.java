package com.neon.diego.neontest.home;

import android.content.Context;

import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.service.BaseService;
import com.neon.diego.neontest.utils.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeInteractor implements HomeContract.Interactor {

    private Context mContext;
    private Call<String> generateToken;

    public HomeInteractor(Context context) {
        mContext = context;
    }

    @Override
    public User getUser() {
        return createSelfUser();
    }

    @Override
    public void getToken(User user) {
        generateToken = BaseService.getTokenService().getToken(user.getName(), user.getEmail());
        generateToken.enqueue(getToken);
    }

    private Callback<String> getToken = new Callback<String>() {

        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            String token = response.body();
            Preferences.saveToken(mContext, token);
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
        }
    };

    private User createSelfUser() {
        User user = new User();
        user.setName("Diego Figueiredo");
        user.setEmail("diego@email.com");
        user.setPhone("(11) 96425-1305");
        user.setPhoto("http://listen-hard.com/wp-content/uploads/2014/11/happy-man1.jpg");
        return user;
    }

}
