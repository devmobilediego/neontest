package com.neon.diego.neontest.send;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.neon.diego.neontest.R;
import com.neon.diego.neontest.model.User;
import com.neon.diego.neontest.ui.dialogs.SendMoneyDialog;
import com.neon.diego.neontest.utils.InitialsName;
import com.neon.diego.neontest.ui.widgets.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SendAdapter extends RecyclerView.Adapter<SendAdapter.ViewHolder> {

    private Context mContext;
    private List<User> mUserList;
    private static final String DIALOG_TAG = "dialog";

    public SendAdapter(Context context, List<User> userList) {
        mContext = context;
        mUserList = userList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_send, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String urlPhoto = mUserList.get(position).getPhoto();
        if (urlPhoto != null && !urlPhoto.equals("")) {

            Picasso.with(mContext)
                    .load(mUserList.get(position).getPhoto())
                    .placeholder(R.drawable.ic_person)
                    .error(R.drawable.ic_person)
                    .transform(new CircleTransform())
                    .into(holder.imageView);

            holder.imageView.setVisibility(View.VISIBLE);
            holder.initials.setVisibility(View.GONE);
        } else {
            setInitials(holder, mUserList.get(position));
        }

        holder.name.setText(mUserList.get(position).getName());
        holder.phone.setText(mUserList.get(position).getPhone());

        holder.itemView.setOnClickListener(onViewClicked((mUserList.get(position))));
    }

    private void setInitials(ViewHolder holder, User user) {
        holder.imageView.setVisibility(View.GONE);
        holder.initials.setVisibility(View.VISIBLE);
        holder.initials.setText(getInitialsName(user.getName()));
    }

    private String getInitialsName(String name) {
        return InitialsName.getInitialsName(name);
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    private View.OnClickListener onViewClicked(final User user) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMoneyDialog dialog = SendMoneyDialog.newInstance(user);
                FragmentManager manager = ((SendActivity) mContext).getSupportFragmentManager(); //its not correct, but in this moment is a fast solution
                dialog.show(manager, DIALOG_TAG);
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_user)
        CircleImageView imageView;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.phone)
        TextView phone;

        @BindView(R.id.initials)
        TextView initials;

        private ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

    }

}
