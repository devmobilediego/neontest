package com.neon.diego.neontest.service;

import com.neon.diego.neontest.utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TokenService {

    @GET(Constants.URL_TOKEN)
    Call<String> getToken(@Query("nome") String name, @Query("email") String email);

}
